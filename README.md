# OSI LAYER

OSI(open system interconnection) model is defined and used to understand how the data is transmitted from one computer to another computer in a computer network.

For example, if two computers are sharing the data using LAN or wifi. This model helps us how the data is transmitting.

Look at the below image for a better understanding.

![OSI Model](osi_model.png)

<br>

In 1984 [ISO](https://www.iso.org/) developed a 7 layered model which is in general we call as **OSI Model Layer**

<br>

![OSI Model](OSI_model.png)

&nbsp;

## 1. Application Layer
It provides services to the web applications using protocols to perform user activities like sending requests and getting the responses from the web applications.

In General words, we can say that an Application layer communicated with the web applications to give a response to the users based on their requests to the web browsers.

In the above paragraph, I've mentioned the term _Protocol_. So, don't worry it means establishing a set of rules to transfer the data from one network to another network.

Some of the examples of protocols are.

1. FTP([File Transfer Protocol](https://www.javatpoint.com/computer-network-ftp)) used for the File Transfer
2. HTTP/HTTPS([HyperText Transfer Protocol](https://www.javatpoint.com/computer-network-http)) used for the web search.
3. [SMTP](https://www.mailmodo.com/guides/smtp/) used for the mail transfer.
4. Telnet([Terminal Network](https://www.javatpoint.com/computer-network-telnet)) used for the virtual terminals.

For Better understanding Please look at the below image.

![Application Layer Protocols](Application-Layer-Protocols.png)

&nbsp;

## 2. Presentation Layer

It is very hard to send data that is large in size and also we need to main security to the data while transferring from sender to receiver. So, All the necessary activities needed to compress the data as well as the encryption of data is done in the Presentation layer.

<br>

![Data Compression and Encryption](Presentation_layer.png)

<br>

As you can see in the above image. The Data is first Preprocessing which means it is converting the normal data into binary format and then it is compressing. 

Wait..! It is very important to secure data from hackers while it is transferring. So, to avoid this we have to encrypt the data and also need to decrypt it before delivering it to the receiver. this can be done by using SSL([Secure Sockets Layer](https://www.kaspersky.com/resource-center/definitions/what-is-a-ssl-certificate)).

&nbsp;

## 3. Session Layer
Till now we are able to communicate with network applications and also prepared the data to transfer.

But, did you ever think about how they connect the two different systems to transfer data. This is done in the Session layer and it is done by verifying the below three methods.

1. Authentication
   
   Authentication is basically knowing the browser who you are and it can be done by providing username & password etc.

2. Authorization
   
   It is the process of verifying by the server whether we have permission to access the data or not. If we do not have any permission to access then we will get a message like _You are not authorized to access this page._

3. Session Management
   
   The session layer keeps a track of the data which is transferred between different network applications which is simply what we call Session Management. In this layer, the data is transferred based on the file formate like image or text each data file we call as a Data Packet.

<br>

Please have a look at the below image for a better understanding.

![Session Layer](session-layer.jpg)

&nbsp;

## 4. Transport Layer

This layer is responsible for end-to-end communication over a network.

In General, the Transport layer Collects data from the network applications and transmits it to the Network layer.

This layer is mainly responsible for 
1. Segmentation
2. Flow Controll
3. Error Control

### Segmentation:
Segmentation is to convert the data into segments where each segment has port num of sender and receiver also segment contains the sequence number for each segment.

### Flow Control:

Flow Control is to balance the rate of the amount of data sending the server to the rate of data received by the user computer.

![Flow Control](Flow%20Control.png)

As you can see in the above image Computer can able to process the data at speed of 10MBPS. But, Server is sending the data at a speed of 50 MBPS. So, control the data flow computer will communicate with the server through the Transport layer o decrease the data flow speed. same will repeat on the other side.

<br>

### Error Control:

Suppose we are sending a request to the browser and it sends the response to us. In meanwhile connection got interrupted and the data is corrupted. So, to repair the corrupted data web browser applies an Auto-Repeat Request.

&nbsp;

## 5. Network Layer

The Network layer is to transmit the data from one computer to another computer located in the different networks.

Basically, the Network layer collects the segments from the Transport layer and converts them into data packets to transfer between the two computers.

![Network Layer](network-layer.jpg)

### IP Packets:

An IP Packet is a data object and is formed by adding the IP addresses of the sender and receiver to the data segments.


The function of the Network layer is **Logical addressing, Routing and Path determination**.

### I. Logical Addressing

1. Logical addressing is to identify the network device using the IP address.

2. A logical address lets you access a network device by using an address that you assign.

3. Logical addresses are created and used by Network layer protocols such as IP or IPX. The Network layer protocol translates logical addresses to MAC addresses.

<br>

### II. Routing

In Routing the data collected in Logical addressing will be delivered to the receiver based on the receiver's IP address.

<br>

### III. Path Determination

In Simple words, Path determination is to find the shortest way to deliver the data.

You can experience the visual representation of Routing and Path Determination Below.

![Path Determine](Path_determnaion.jpeg)

<br>

Note: Hey wait don't think it's over. The Network layer delivers the data Packets between the Network systems only.

To know how the physical delivery of the data to the receiver's computer looks at the next step.

&nbsp;

## 6. Data Link Layer

1. The function of a Data Link Layer is to deliver the data to the receiver's device physically by identifying the MAC address.
2. It collects the **Data Packets** from the Network layer and forms a **Data Frames**
3. Data Frame is formed by appending the MAC addresses of the sender and receiver.
4. MAC address is a unique Id assigned by the manufacturer. It is used to address the MAC and can be found on the Network Interface Card(NIC).

![Data Link Layer](Data_link_layer.png)

&nbsp;

Hey...! you can see in the above image the source and destination address is appended to the data frame.

Finally, The data reached the receiver's device. But remember that the data is in the form of [Frames](https://www.geeksforgeeks.org/framing-in-data-link-layer/). Did you ever think that a user can able to understand and read the encrypted data? Not right. 

So, this can be done in the Physical layer.....!

## 7. Physical Layer

Till now The data is segmented by the Transport layer and converted into packets then formed as frames.

SO, this Framed data is decrypted into signals and transferred to the receiver's computer over media like wire, optical fiber, or wifi can be done by Physical Layer.

![Physical Layer](Physical_layer.jpg)

<br>

Here we go the message is finally reached to the receiver's device securely and this will repeate to send responce from receiver to sender.

<br>

Reference Links:
1. [GeeksforGeeks](https://www.geeksforgeeks.org/layers-of-osi-model/)
2. [Force Point](https://www.forcepoint.com/cyber-edu/osi-model)
3. [Java Point](https://www.javatpoint.com/osi-model)

<br>
<br>

<div align="center"><p style="font-size:60px">***<strong>Thank You</strong>***</p> <div>








